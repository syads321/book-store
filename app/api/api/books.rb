module Api
  class Books < Grape::API
    format :json

    desc 'Retrieve a list of books'
    get do
      present Book.all, with: Api::Entities::Book
    end
  end
end

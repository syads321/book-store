class bookController {
    constructor($scope, $location, AppConstants) {
        'ngInject';
        let vm = this;
        vm._$location = $location;
        vm._AppConstants = AppConstants;
        console.log(vm)
        //
    }
    openLightbox(data) {
        let vm = this;
        vm._$location.path(data.category + '/' +  data.id);
    }
    $onInit() {
        let vm = this;
        vm.pdfurl = vm._AppConstants.host + vm.data.pdf;
    }
    
}
export default bookController;
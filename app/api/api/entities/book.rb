module Api
  module Entities
    class Book < Grape::Entity
      expose :id
      expose :title
      expose :description
      expose :category
      expose :cover do |book, _options|
        book.pdf.url(:cover)
      end
      expose :pdf do |book, _options|
        book.pdf.url
      end
      expose :html do |book, _options|
        book.html.url
      end
    end
  end
end

angular.module("app.run", []).run(function($rootScope, $route, $location) {
    
    // Send url changes event to components
    $rootScope.$on('$locationChangeSuccess', function () {
         $rootScope.$emit('urlChange', $route.current.params);
    });

});


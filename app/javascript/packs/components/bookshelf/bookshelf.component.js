import bookshelfCtrl from './bookshelf.controller';

let Bookshelf = {
    bindings: {
      data: '=',
    },
   controller:  bookshelfCtrl,                  
   templateUrl: 'bookshelf.template.html'
  };
  
export default Bookshelf;

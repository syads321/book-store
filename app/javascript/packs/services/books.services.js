export default class Books {
    constructor($http, $q, AppConstants) {
      'ngInject';
  
      this._AppConstants = AppConstants;
      this._$http = $http;
      this._$q = $q;
  
  
    }
    get() {
      let deferred = this._$q.defer();
      this._$http({
        url: this._AppConstants.api + '/books',
        method: 'GET'
      }).then(
        (res) => deferred.resolve(res.data),
        (err) => deferred.reject(err)
      );
  
      return deferred.promise;
    }
  
  }
  
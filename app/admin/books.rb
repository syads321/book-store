ActiveAdmin.register Book do
  menu label: 'Books', priority: 1
  permit_params :title, :description, :author, :pdf, :category
  filter :title
  filter :description
  filter :author
  filter :category
  filter :created_at
  filter :updated_at

  scope('All', default: true) { |s| s }
  scope('Sports', default: true) { |s| s.where(category: 'sports') }
  scope('News', default: true) { |s| s.where(category: 'news') }
  scope('Business', default: true) { |s| s.where(category: 'business') }

  index do
    id_column
    column :thumbnail do |book|
      image_tag(book.pdf.url(:thumb))
    end
    column :title
    column :category
    column :status
    column :pdf do |book|
      link_to(book.pdf_file_name, book.pdf.url, target: '_blank')
    end
    column :html do |book|
      link_to(book.html_file_name, book.html.url, target: '_blank')
    end
    column :author
  end

  show do |book|
    attributes_table do
      row :title
      row :description
      row :category
      row :author
      row :status
      row :created_at
      row :updated_at
      row :pdf do
        link_to(book.pdf_file_name, book.pdf.url, target: '_blank')
      end
      row :html do
        link_to(book.html_file_name, book.html.url, target: '_blank')
      end
    end
  end

  form do |f|
    f.semantic_errors
    f.inputs 'Listing' do
      f.input :title
      f.input :description
      f.input :category, as: :select, collection: %w[sports news business]
      f.input :author
      f.input :pdf, required: true, as: :file
    end
    f.actions
  end
end

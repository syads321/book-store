require_relative 'boot'

require 'rails'
require 'active_model/railtie'
require 'active_job/railtie'
require 'active_record/railtie'
require 'action_controller/railtie'
require 'action_view/railtie'
require 'sprockets/railtie'
require 'rails/test_unit/railtie'

Bundler.require(*Rails.groups)

module BookStore
  class Application < Rails::Application
    config.load_defaults 5.1
    config.active_job.queue_adapter = :resque
    config.paths.add(File.join('app', 'api'), glob: File.join('**', '*.rb'))
    config.autoload_paths += Dir[Rails.root.join('app', 'api', '*')]
  end
end

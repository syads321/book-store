angular.module("app.lightbox", [])
.config(function(LightboxProvider) {
    LightboxProvider.templateUrl = 'lightbox.html';
});
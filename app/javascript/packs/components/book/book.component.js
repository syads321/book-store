import bookCtrl from './book.controller';

let Bookshelf = {
    bindings: {
      data: '=',
    },
   controller:  bookCtrl,                        
   templateUrl: 'book.template.html'
  };
  
export default Bookshelf;

/* eslint no-console:0 */

// Vendor Scripts
import 'angular'
import 'angular-material'
import 'angular-resource'

//Libs
import './libs/material.lightbox/material.lightbox'
import './libs/angular-material-icons/angular-material-icons.min'

//Contants
import constants  from './config/app.constant';

// Stylesheets
import '../app/scss/application'

// Configuration files
import './config/app.routes.js'
import './config/app.run.js'
import './config/app.lightbox.js'

// Services
import './services/'

// Componentst 
import './components'


// App Modules
const requires = [
    'ngMaterial',
    'ngMdIcons',
    'mdLightbox',
    'app.routes',
    'app.components',
    'app.run',
    'app.services',
    'app.lightbox'
]


const app = angular.module('app', requires) 
app.constant('AppConstants', constants);

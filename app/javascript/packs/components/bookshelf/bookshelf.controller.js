class bookshelfController {
    constructor($scope, $routeParams, $rootScope, $location, AppConstants, Lightbox, Books) {
        'ngInject';
        let vm = this;
        vm._$routeParams = $routeParams;
        vm._$rootScope = $rootScope;
        vm._$location = $location;
        vm._Lightbox = Lightbox;
        vm._Books = Books;
        vm._AppConstants = AppConstants;
        vm.books = [];
        vm.filters = ['sports', 'news', 'bussiness'];
    }
    $onInit() {
        let vm = this;
        vm._Books.get().then(function(data) {
            vm.books = data;
            vm.booklist = data.filter(function(obj) {
                return obj.category == vm._$routeParams.type;
            });
            if (typeof vm._$routeParams.id !== 'undefined') {
                const data =  vm.booklist.filter(function( obj ) {
                    return obj.id == vm._$routeParams.id;
                });
                var url = vm._AppConstants.host + data[0].html;
                vm._Lightbox.openModal(url, vm._$routeParams.type)
            }
        })  

        // Watch url changes
        vm._$rootScope.$on('urlChange', function (event, data) {
            vm.activetabs = vm.filters.indexOf(data.type);
            vm.booklist = vm.books.filter(function(obj) {
                return obj.category == data.type;
            });
            if (typeof data.id !== 'undefined') {
                const books =  vm.booklist.filter(function( obj ) {
                   if (typeof obj !== 'undefined') {
                        return parseInt(obj.id) == parseInt(data.id);
                   }
                });
                if(books.length == 1) {
                     var url = vm._AppConstants.host + books[0].html;
                    vm._Lightbox.openModal(url, data.type)
                }
            }
            else {
                vm._$rootScope.$emit('forceCloseLightBox');
            }
        })
    }
    endSelected(data) {
        let vm = this;
       vm._$location.path(data);
    }
 
    
}
export default bookshelfController;
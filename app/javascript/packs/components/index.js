let componentModule = angular.module('app.components', []);
import bookshelf  from './bookshelf/bookshelf.component.js';
import book  from './book/book.component.js';

componentModule.component('bookshelf', bookshelf);
componentModule.component('book', book);
export default componentModule;
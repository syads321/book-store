Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  mount Api::Books => '/api/books'
  root to: 'home#index'
end

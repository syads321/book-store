class ConvertBookJob < ApplicationJob
  queue_as :default

  def perform(book)
    book.update_column(:status, 'processing')
    Tempfile.open('convert-book', 'tmp/jobs') do |file|
      Kristin.convert(book.pdf.path, file.path)
      book.html = file
      book.html_file_name = book.pdf_file_name.gsub(/\.pdf$/, '.html')
      book.status = 'converted'
    end
    book.save!
  end
end

class Book < ActiveRecord::Base
  has_attached_file :html
  validates_attachment_file_name :html, matches: [/[a-z0-9-]+\.html$/]
  validates_uniqueness_of :html_file_name, allow_blank: true, allow_nil: true

  has_attached_file :pdf, styles: { thumb: ['140x140>', :jpg], cover: ['512x512>', :jpg] }
  validates_attachment_file_name :pdf, matches: [/[a-z0-9-]+\.pdf$/]
  validates_uniqueness_of :pdf_file_name, allow_blank: true, allow_nil: true

  after_create do
    ConvertBookJob.perform_later(self)
  end
end


import BookService from './books.services';
let servicesModule = angular.module('app.services', []);
servicesModule.service('Books', BookService);

namespace :convert do
  task book: :environment do
    book = Book.last
    ConvertBookJob.perform_later(book)
    puts "- queued conversion of #{book.title}"
  end
end

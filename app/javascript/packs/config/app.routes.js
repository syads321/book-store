import 'angular'
import 'angular-route'

angular.module("app.routes", ["ngRoute"])
.config(function($routeProvider, $locationProvider) {
    const bookShelfPage = {
      template: '<div layout="row" layout-wrap><book data="item" ng-repeat="item in $ctrl.booklist"></book></div>',
    }

    // Build multilevel page using native angular-route
    $routeProvider.when('/:type', bookShelfPage)
    $routeProvider.when('/:type/:id', bookShelfPage)
    .otherwise({ redirectTo: '/sports' });
});